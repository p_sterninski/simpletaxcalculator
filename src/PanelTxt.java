import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;



/**
 * Panel zawierający pola tekstowe. Za poprawność danych wpisanych w pola tekstowe (wartość musi być liczbą) 
 * odpowiada obiekt klasy InputVerifier, jeśli dane są błędne zmiania wygląd pola tekstowego i uniemożliwia przejście o innego pola.
 * @author Przemek
 *
 */
public class PanelTxt
	extends JPanel{
	
	
	/**
	 * Konstruktor 
	 * @param nazwa nazwa kolumny z polami tekstowymi (znajdujaca się w nagłówku)
	 * @param list lista zawierajaca pola tekstowe
	 * @param szerokosc szerokosc pola tekstowego
	 * @param czyEdit czy pola sa edytowalne
	 * @param typ skrot opisujacy panel
	 * @param liczbaPolTxt liczba pol tekstowych w panelu (łącznie z podsumowaniem)
	 * @param bOblicz przycisk do obliczania podatku
	 * @param lPodatek etykieta z wartoscia podatku
	 */
	public PanelTxt(String nazwa, ArrayList<JTextField> list, int szerokosc, boolean czyEdit, String typ, int liczbaPolTxt, JButton bOblicz, JLabel lPodatek) {
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); //uzyto BoxLayout poniewaz eytkieta z nazwa kolumny ma inna wysokosc niż pozostale elementy
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		
		String nazwa2 = nazwa.replaceAll("<br>", " ").replaceAll("<[^>]*>", ""); //usunięcie w zmiennej nazwa znacznikow HTML
		
		JLabel tytul = new JLabel(nazwa, SwingConstants.CENTER); //nazwa kolumny z polami JTextField
		
		this.add(tytul);
		
		for(int i = 0; i < liczbaPolTxt; i++){
			JTextField txt = new JTextField(szerokosc);
			txt.setHorizontalAlignment(JTextField.RIGHT);
			
			//sprawdza czy wartość w polu tekstowym jest liczbą, jesli nie to zmiania kolor komórki na żółty, 
			//w polu podatku wpisuje "???" i blokuje przycisk Oblicz oraz uniemożliwia zmianę fokusu. 
			//Obiekt jest dodawany w metodzie utworzPanelPolaTxt
			txt.setInputVerifier(new InputVerifier() {
														private boolean check = true;
														
													    public boolean verify(JComponent c) {         
													      String txt = ((JTextField) c).getText();    
										
													      try {                                       
													  		if (!txt.equals("")) {
																Double.parseDouble(txt);
													  		}
													      } catch (NumberFormatException exc) { 
													    	if(check==true) {
														    	check = false;
														        c.setBackground(Color.YELLOW);
														        lPodatek.setText("???");
														        bOblicz.setEnabled(false);    
													    	}
													    	return false;
													      }
													      if(check==false){
													    	  check = true;
													    	  bOblicz.setEnabled(true);
													    	  lPodatek.setText("");
													      }
													      return true;
													    }
													}); //sprawdzenie czy wartosci sa typu Double, zmiana tla jesli nie sa
			
			//okreslenie czy pole tekstowe jest edytowalne, z reguly wszystkie sa poza podsumowaniem (i==nrOstElement) ale wyjatkiem jest Dochod
			//dla listy listaKupProc dodanie menu kontekstowego
			//dodanie do ClientProperty wartosci, ktora ma sie wyswietlic na pasku stanu
			if(i < (liczbaPolTxt-1)) {
				txt.setEditable(czyEdit);
				txt.putClientProperty("nazwa", nazwa2 + " ze źródła: Źródło " + (i+1));
				
				if(typ.equals("KupProc")) {
					txt.setComponentPopupMenu(utworzPopup("0", "20", "50"));
				}
			}
			else{
				if(typ.equals("KupProc")) {
					txt.setEnabled(false);
					txt.setBackground(Color.GRAY);
				}else
					txt.setEditable(false);
					txt.putClientProperty("nazwa", nazwa2 + " ze źródła: Suma wszystkich źródeł");
			}
			
			if(txt.isEditable() == false)
				txt.setForeground(Color.BLUE);
				
			txt.putClientProperty("zrodlo", i); //okreslenie wiersza listy, przydatne przy liczeniu wartosci z pozostalych list dla tego samego wiersza(zrodla)
			txt.putClientProperty("typ", typ); //skrot opisujacy liste (aby nie koarzystac z calej nazwy)
			txt.putClientProperty("tlo", txt.getBackground()); //tlo poczatkowe, do przywrocenia przy zdjeciu focusu
			txt.putClientProperty("rama", txt.getBorder()); //rama poczatkowa, do przywrocenia przy zdjeciu focusu
			
			list.add(txt);
			this.add(txt);
		}	
		
		tytul.setPreferredSize(new Dimension(list.get(0).getWidth(), 60));
		tytul.setOpaque(true);
	}
	
	
	
	//Tworzy popup menu z opcjami wyboru dla pól tekstowych z kolumny "koszty uzyskania w %"
	//Obiekt jest dodawany w metodzie utworzPanelPolaTxt
	private JPopupMenu utworzPopup(String ... pola) {
		JPopupMenu popup = new JPopupMenu();
		for(String s : pola) {
			popup.add(new JMenuItem(s));
		}
		return popup;
	}

}



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;


/**
 * Klasa CalcView reprezentuje wygląd kalkulatora podatkowego. 
 * @author Przemysław Sterniński
 *
 */
public class CalcView extends JFrame {
	

	//lista elementow JTextField, ktorych wartosci sluza do obliczenia podatku
	//ostatnim elementem kazdej listy jest podsumowanie, w ktorym jest suma innych elementow
	private final ArrayList<JTextField> listaPrzychod = new ArrayList<JTextField>();
	private final ArrayList<JTextField> listaKupProc = new ArrayList<JTextField>();
	private final ArrayList<JTextField> listaKup = new ArrayList<JTextField>();
	private final ArrayList<JTextField> listaDochod = new ArrayList<JTextField>();
	private final ArrayList<JTextField> listaZaliczki = new ArrayList<JTextField>();
	
	private final int liczbaPolTxt; //liczba wierszy pol tekstowych, lacznie z podsumowaniem.
	private final int nrOstElement; //numer wiersza(zrodla) z podsumowaniem
	
	private final JLabel lPodatek = new JLabel(); //etykieta z wartoscia podatku
	private final JButton bOblicz = new JButton("Oblicz"); 
	private final JCheckBox chWiersze = new JCheckBox("Wiersza");
	private final JCheckBox chPodatek = new JCheckBox("Podatku");

	private final JLabel lPasekStanu = new JLabel();
	
	
	
	/**
	 * Konstruktor przyjmuje jako parametr liczbę weirszy w kalkulatorze (łącznie z podsumowaniem)
	 * @param liczbaPolTxt liczba wierszy w kalkulatorze (łącznie z podsumowaniem)
	 */
	public CalcView(int liczbaPolTxt) {
		super("Kalkulator podatkowy v1.0");
		
		this.liczbaPolTxt = liczbaPolTxt;
		this.nrOstElement = liczbaPolTxt - 1;
		
		JPanel pTxtAll = utworzPanelTxtAll(); //gorna czesc aplikacji z polami JTextField
		JPanel pPodatek = utworzPanelObliczPodatek(); //srodkowa czesc aplikacji z wartoscia podatku
		JPanel pStan = utworzPanelStanu(); //dolna czesc aplikacji z paskiem stanu
		
		JPanel pDolnaStalaCzesc = new JPanel(); //panel roboczy, służy do połaczenie pPodatek i pStan aby ich rozmiary nie zmieniały się przy zmianie rozmiarow okna
		pDolnaStalaCzesc.setLayout(new BoxLayout(pDolnaStalaCzesc, BoxLayout.Y_AXIS));
		pDolnaStalaCzesc.add(pPodatek);
		pDolnaStalaCzesc.add(pStan);
		
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(pTxtAll, "Center");
		cp.add(pDolnaStalaCzesc, "South");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		//setVisible(true);
	}
	

	
	//metoda która zwraca główny panel z wszystkimi polami tekstowymi
	private JPanel utworzPanelTxtAll(){
		JPanel pTxtAll = new JPanel();
		
		JPanel pPrzychod = new PanelTxt("<html>Przychód</html>", listaPrzychod, 10, true, "Przychod", liczbaPolTxt, bOblicz, lPodatek);
		JPanel pKupProc = new PanelTxt("<html><center>Koszty<br>uzyskania<br>w %</center></html>", listaKupProc, 8, true, "KupProc", liczbaPolTxt, bOblicz, lPodatek);
		JPanel pKup = new PanelTxt("<html><center>Koszty<br>uzyskania<br>przychodu</center></html>", listaKup, 10, true, "Kup", liczbaPolTxt, bOblicz, lPodatek);
		JPanel pDochod = new PanelTxt("<html><center>Dochód</center></html>", listaDochod, 10, false, "Dochod", liczbaPolTxt, bOblicz, lPodatek);
		JPanel pZaliczki = new PanelTxt("<html><center>Zaliczki</center></html>", listaZaliczki, 10, true, "Zaliczki", liczbaPolTxt, bOblicz, lPodatek);
		
		
		pTxtAll.setBorder(BorderFactory.createRaisedBevelBorder());
		pTxtAll.setLayout(new FlowLayout());
		
		pTxtAll.add(pPrzychod);
		pTxtAll.add(pKupProc);
		pTxtAll.add(pKup);
		pTxtAll.add(pDochod);
		pTxtAll.add(pZaliczki);
		
		return pTxtAll;
	}
	
	
	
	// tworzy środkowy panel z przyciskiem Oblicz i wartością podatku oraz opcjami wyboru sposobu przeliczenia.
	private JPanel utworzPanelObliczPodatek(){
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		p.setBorder(BorderFactory.createLoweredBevelBorder());
		
		lPodatek.setPreferredSize(new Dimension(170, 45));
		lPodatek.setBackground(new Color(255, 255, 230));
		lPodatek.setHorizontalAlignment(SwingConstants.RIGHT);
		lPodatek.setOpaque(true);
		lPodatek.setBorder(BorderFactory.createCompoundBorder(
															BorderFactory.createLineBorder(Color.RED, 4), 
															BorderFactory.createTitledBorder(
																							BorderFactory.createLineBorder(Color.RED),
																							"Wyliczony podatek"	
																							)
															)
							);
				
		JPanel pOpcjeWyboru = new JPanel(); //panel roboczy przechowujacy checkbox'y
		pOpcjeWyboru.setBorder(BorderFactory.createTitledBorder("Automatyczne przeliczenia"));
		pOpcjeWyboru.add(chWiersze);
		pOpcjeWyboru.add(chPodatek);
		
		
		p.add(bOblicz);
		p.add(lPodatek);
		p.add(pOpcjeWyboru);
		
		return p;
		
	}
	
	
	
	//Tworzy dolny panel pokazujcym, które pola teksowe są aktualnie wybrane
	private JPanel utworzPanelStanu(){
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(p.getWidth(), 30));
		p.setLayout(new GridLayout(1, 1));
		p.setBorder(BorderFactory.createRaisedBevelBorder());
		
		lPasekStanu.setHorizontalAlignment(SwingConstants.LEFT);
		
		p.add(lPasekStanu);	
		
		return p;
	}
	
	
	
	public ArrayList<JTextField> getListaPrzychod() {
		return listaPrzychod;
	}


	
	public ArrayList<JTextField> getListaKupProc() {
		return listaKupProc;
	}

	
	
	public ArrayList<JTextField> getListaKup() {
		return listaKup;
	}

	

	public ArrayList<JTextField> getListaDochod() {
		return listaDochod;
	}


	
	public ArrayList<JTextField> getListaZaliczki() {
		return listaZaliczki;
	}	
	
	
	
	public int getLiczbaPolTxt() {
		return liczbaPolTxt;
	}


	
	public int getNrOstElement() {
		return nrOstElement;
	}

	
	
	public JLabel getlPodatek() {
		return lPodatek;
	}


	
	public void setlPodatek(String podatek) {
		this.lPodatek.setText(podatek);
	}
	
	
	
	public JLabel getlPasekStanu() {
		return lPasekStanu;
	}


	
	public void setlPasekStanu(String txt) {
		this.lPasekStanu.setText(txt);
	}
	

	
	public JCheckBox getChWiersze() {
		return chWiersze;
	}


	
	public JCheckBox getChPodatek() {
		return chPodatek;
	}


	
	public JButton getbOblicz() {
		return bOblicz;
	}
	
}

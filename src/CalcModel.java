


/**
 * Klasa CalcModel opisuje model prostego kolakulatora podatkowego.
 * Uwzględnia metody służące do wyliczenia kosztu uzyskania przychodu, dochodu i podatku.
 * @author Przemysław Sterniński
 *
 */
public class CalcModel {
	
	private final double podatekProc;
	
	
	
	/**
	 * Konstruktor
	 * @param podatekProc procentowa wartość podatku (podana jako ułamek dziesiętny), wykorzystywana w metodzie obliczPodatek
	 */
	public CalcModel(double podatekProc) {
		
		this.podatekProc = podatekProc;
		
	}
	
	
	
	/**
	 * Metoda obliczająca koszt uzyskania przychodu jako iloczyn przychodu o procentowego kosztu uzyskania przychodu.
	 * @param przychod wartość przychodu
	 * @param kupProc procentowa wartość kosztu uzyskania przychodu (podana jako ułamek dziesiętny)
	 * @return zwraca koszt uzyskania przychodu
	 */
	public double obliczKup(double przychod, double kupProc){
		return przychod * kupProc;
	}
	
	
	
	/**
	 * Dochod liczony jako różnica przychodu i kosztu uzyskania przychodu.
	 * @param przychod wartość przychodu
	 * @param kup wartosc kosztu uzyskania przychodu
	 * @return zwraca dochód
	 */
	public double obliczDochod(double przychod, double kup) {
		return przychod - kup;
	}
	
	
	
	/**
	 * Metoda wylicza podatek według wzoru: podatekProc * dochod - zaliczka.
	 * @param dochod wartość dochodu
	 * @param zaliczka wartość zaliczki
	 * @return zwraca podatek
	 */
	public double obliczPodatek(double dochod, double zaliczka){
		return podatekProc * dochod - zaliczka;
	}

}

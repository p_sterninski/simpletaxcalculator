

import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.border.Border;


/**
 * Klasa CalcContoller jest mostem łączącym model i widok kalkulatora podatkowego, oba te obiekty muszą być podane w konstruktorze. 
 * Obiekt CalcController dodaje nasłuch na 
 * - pozyskanie i utratę focusu dla pól tekstowych
 * - menu kontekstowe dla pól w kolumnie "koszty uzysknia w %", umożliwiając szybkie wybranie jednej z proponowanych wartości
 * - przycisk Oblicz, w celu wyliczenia podatku.
 * @author Przemysław Sterniński
 *
 */
public class CalcController {
	
	private final CalcModel model;
	private final CalcView widok;
	
	
	/**
	 * Konstruktor przyjmujacy jako parametry obiekty: model (klasy CalcModel) i widok (klasy CalcView). 
	 * @param model model kalkulatora podatkowego
	 * @param widok widok kalkulatora podatkowego
	 */
	public CalcController(CalcModel model, CalcView widok) {
		this.model = model;
		this.widok = widok;
		
		dodajFocus(widok.getListaDochod());
		dodajFocus(widok.getListaPrzychod());
		dodajFocus(widok.getListaKup());
		dodajFocus(widok.getListaZaliczki());
		dodajFocus(widok.getListaKupProc());
		
		ListIterator<JTextField> iterator = widok.getListaKupProc().listIterator();
		while(iterator.hasNext() && iterator.nextIndex() < widok.getNrOstElement()) {
			JTextField txt = iterator.next();
			for(Component c : txt.getComponentPopupMenu().getComponents()) {
				JMenuItem item = ((JMenuItem) c);
				item.addActionListener(
										e -> {
											
											if(!txt.getText().equals(item.getActionCommand())){
												txt.setText(item.getActionCommand());
											
												if(widok.getChPodatek().isSelected()) {
													obliczPodatek();
												}
												else if(widok.getChWiersze().isSelected()) {
													int zrodlo = (Integer)txt.getClientProperty("zrodlo");
													przeliczZrodla(zrodlo, zrodlo);
												}
											}
										});
				
			}
		}
		
		widok.getbOblicz().addActionListener(e -> obliczPodatek());		
		
		
	}
	
	
	//zwraca wartość liczbową z tekstowej (jeśli wartość tekstowa jest pusta, to zwraca 0);
	private double zwrocDouble(String txt){
		if (txt.equals(""))
			return 0.0;
		else
			return Double.parseDouble(txt);
	}
	
	
	//wstawia zera do list jesli ich elementy sa rowne "" (mozna wybrac zakres wierszy(zrodel)
	private void wstawZera(int zrodloOd, int zrodloDo, ArrayList<JTextField>... kolumna){
		for(ArrayList<JTextField> lista : kolumna){
			for(int i = zrodloOd; i <= zrodloDo; i++){
				if(lista.get(i).getText().equals("")) lista.get(i).setText("" + 0.0);
			}
		}
		
	}
	
	
	//wylicza koszt uzysk. przychodu na podstawie przychodu i kosztu %
	//jesli pole z Przychodem lub z kosztem w % jest puste lub równe 0, to nie powinien byc ponownie wyliczany koszt wartosciowy
	// ale jesli w takiej sytuacji koszt wartosciowy jest rozny od "" lub 0 to powinien byc przeliczony (bo jest to blad)
	private String obliczKup(int zrodlo){
		double przychod = zwrocDouble(widok.getListaPrzychod().get(zrodlo).getText());
		double kupProc = zwrocDouble(widok.getListaKupProc().get(zrodlo).getText());
		
		if(przychod == 0.0 || kupProc == 0.0) {
			if(zwrocDouble(widok.getListaKup().get(zrodlo).getText()) == 0.0) {
				return widok.getListaKup().get(zrodlo).getText();
			}
		}
				
		double wart = model.obliczKup(przychod, kupProc/100);
		
		return String.format(Locale.US, "%.2f", wart);
	}
	
	
	//wylicza dochod na podstawie przychodu i kosztu uzyskania przychodu
	private String obliczDochod(int zrodlo){
		String kup = obliczKup(zrodlo);
		
		if(!kup.equals(widok.getListaKup().get(zrodlo).getText())) //sprawdzenie czy wartosc kosztu uzysk. przychodu jest poprawna
			widok.getListaKup().get(zrodlo).setText(kup);			// jeśli nie to ją aktualizuje
		
		double przychod =  zwrocDouble(widok.getListaPrzychod().get(zrodlo).getText());
		if(przychod != 0){ //jesli przychod rozny od "" i 0.0 to oblicz dochod, w przeciwnym razie zwroc obecny dochod
			double wart = model.obliczDochod(przychod, zwrocDouble(kup));
			return String.format(Locale.US, "%.2f", wart);
		}else
			return widok.getListaDochod().get(zrodlo).getText();
	}
	
	
	
	//sumuje wartosci z danej listy i wstawia do podsumowania (ostatni element listy)
	private void podsumuj(ArrayList<JTextField> lista){
		double suma = 0.0;
		for(int i = 0 ; i < widok.getNrOstElement() ; i++){	// bez otatniego elementu - lista.get(liczbaPolTxt-1) - poniewaz jest w nim podsumowanie
			suma = suma + zwrocDouble(lista.get(i).getText());
		}
		
		lista.get(widok.getNrOstElement()).setText(String.format(Locale.US, "%.2f", suma)); //wstawienie sumy listy do podsumowania
		
	}
	
	
	
	//przelicza Koszty uzysk. przychodu i Dochod, jesli dla tych list jest wpisana wartosc "" to zmienia na 0.0
	//przelicza podsumowania dla wszystkich kolumn
	private void przeliczZrodla(int zrodloOd, int zrodloDo){
		for(int i = zrodloOd ; i <= zrodloDo ; i++){
			widok.getListaKup().get(i).setText(obliczKup(i));
			wstawZera(zrodloOd, zrodloDo, widok.getListaKup());
			
			widok.getListaDochod().get(i).setText(obliczDochod(i));
			wstawZera(zrodloOd, zrodloDo, widok.getListaDochod());
		}
		
		podsumuj(widok.getListaPrzychod());
		podsumuj(widok.getListaKup());
		podsumuj(widok.getListaDochod());
		podsumuj(widok.getListaZaliczki());
	}
	
	
	//Oblicza podatek i wstawia wartosc do etykiety lPodatek
	//jesli w aplikacji sa pola "" to zamienia je na 0.0
	private void obliczPodatek(){
		
		przeliczZrodla(0, widok.getNrOstElement()-1);
		wstawZera(0, widok.getNrOstElement()-1, widok.getListaPrzychod(), widok.getListaKupProc(), widok.getListaZaliczki()); //bez listaKup i listaDochod ponieważ są przeliczane w metodzie przeliczZrodla()
		
		double podatek = model.obliczPodatek(zwrocDouble(widok.getListaDochod().get(widok.getNrOstElement()).getText()) 
								, zwrocDouble(widok.getListaZaliczki().get(widok.getNrOstElement()).getText()));
		
		widok.setlPodatek(String.format(Locale.US, "%.2f", podatek));
	}
	
	
	//przywrocenie poczatkowych ustawien (patrz metoda utworzPanelPolaTxt()) przy zdjeciu focusu
	private void wygladNorma(JTextField c){
		c.setBorder((Border)c.getClientProperty("rama")); 
		c.setBackground((Color)c.getClientProperty("tlo"));
	}

	
	//zmiana wygladu parametrow przy nadaniu focusu
	private void wygladFocus(JTextField c){
		if(c.isEditable())
			c.setBorder(BorderFactory.createEtchedBorder(Color.ORANGE, Color.RED));
		else
			c.setBorder(BorderFactory.createEtchedBorder(Color.CYAN, Color.BLUE));
	}
	
	
	private void dodajFocus(List<JTextField> lista) {
		for(JTextField el : lista) {
			el.addFocusListener(new Focus());
		}
	}
	
	
	private class Focus implements FocusListener{

		@Override
		public void focusGained(FocusEvent fe) {
			JTextField txt = (JTextField)fe.getSource();
			wygladFocus(txt);
			
			widok.setlPasekStanu((String)txt.getClientProperty("nazwa"));
			txt.putClientProperty("old", txt.getText()); //wpisuje do ClientProperty obecna wartosc pola
			int zrodloFocus = (Integer)txt.getClientProperty("zrodlo");
			
			if(zrodloFocus == widok.getNrOstElement()) return; //nie ma potrzeby aby przeliczac ponizszy kod jesli jestesmy w wierszu z podsumowaniem
			
			String wynik;
			
			if("Kup".equals((String)txt.getClientProperty("typ"))){ //jesli pole jest Koszt. uzysk. przych to nastepuje przeliczenie jego wartosci
				wynik = obliczKup(zrodloFocus);
				txt.setText(wynik);
				if(!wynik.equals(""))
					podsumuj(widok.getListaKup());
			} else if("Dochod".equals((String)txt.getClientProperty("typ"))){ //jesli pole jest Dochodem to nastepuje przeliczenie jego wartosci
					wynik = obliczDochod(zrodloFocus);
					txt.setText(wynik);
					if(!wynik.equals("")){
						podsumuj(widok.getListaKup());
						podsumuj(widok.getListaDochod());
					}
			}
			
		}

		@Override
		public void focusLost(FocusEvent fe) {
			JTextField txt = (JTextField)fe.getSource();
			wygladNorma(txt);
			
			int zrodloFocus = (Integer)txt.getClientProperty("zrodlo");
			
			//jesli jest zaznaczony checkbox i wartosc pola jest inna niż przy nadaniu focusu to ponizszy kod jest realizowany
			if(!txt.getText().equals(txt.getClientProperty("old"))){
				if(widok.getChPodatek().isSelected())
					obliczPodatek();
				else if(widok.getChWiersze().isSelected())
					przeliczZrodla(zrodloFocus, zrodloFocus);	
			}
			
		}
		
	}
}

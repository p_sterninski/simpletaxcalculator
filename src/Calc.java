
/**
 * Program przedstawia prosty kalkulator podatkowy. Dane do wyliczenia podatku
 * (przychód, koszty uzyskania w %, koszty uzyskania przychodu, zaliczki)
 * są podawane w odpowienidch panelach pól tekstowych (ostatnie pole każdego panelu
 * jest nieedytowalne - pokazuje podsumowanie). Panel z dochodami jest nieedytowalny,
 * wartości w nim są wyliczane z innych pól. W panelu "koszty uzyskania w %"
 * znajduje się menu kontekstowe z opcjami: 0, 20, 50.
 * 
 * Obliczenia w programie (znajdują się w klasie CalcModel):
 * 1) koszt uzyskania przychodu = przychód * koszty uzyskania przychodu w %
 * 2) dochód = przychód - koszt uzyskania przychodu
 * 3) podatek = 0.15 * dochód - zaliczka (wartość % podatku (0.15) może zostać zmieniona w kontruktorze CalcModel).
 * 
 * Uzyskanie fokusu przez pola "koszty uzyskania przychodu" powoduje automatyczne
 * przeliczenie wartości, zgodnie z wzorem 1). Uzyskanie fokusu przez pola z dochodami
 * uruchamia wzór 2).
 * 
 * Za weryfkikację wporwadzonych wartości (czy są liczbami) odpowiada obiekt InputVerifier.
 * Jeśli nastąpi błąd, zmienia się kolor pola tekstowego i nie można go opuścić, aż do poprawienia.
 * 
 * Możliwe jest automatyczne przeliczenie wiersza lub całego formularza po zmianie fokusu,
 * poprzez włączenie odpowiedniego znacznika (Wiersza/Podatku) w środowej części programu.
 * 
 * W dolnej części programu pokazywana jest informacja o polu, które aktualnie ma fokus.
 * 
 * @author Przemysław Sterniński
 *
 */
public class Calc {

  public static void main(String[] args) {
	  
	  CalcModel model = new CalcModel(0.15);
	  CalcView widok = new CalcView(5);
	  CalcController kontroler = new CalcController(model, widok);
	  
	  widok.setVisible(true);
	  
  }
}
